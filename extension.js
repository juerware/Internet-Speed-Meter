/*
* Name: Internet Speed Meter
* Version: 1.2
* Description: A simple and minimal internet speed meter for Gnome Shell.
* Author: @juerware
* GitLab: https://gitlab.com/juerware/Internet-Speed-Meter
* License: GPLv3.0
*/

const St = imports.gi.St;
const Main = imports.ui.main;
const Gio = imports.gi.Gio;
const Mainloop = imports.mainloop;
const refreshTime = 1.0; // Set refresh time to one second.

let prevBytes = 0.0, prevBytesReceive = 0.0, prevBytesTransmit = 0.0, prevMemPercent = 0.0, prevCpuPercent = 0.0, prevCpuIdle, prevCpuTotal = 0.0;

function getNetSpeed() {
  try {
    let file = Gio.file_new_for_path('/proc/net/dev');
    let fileStream = file.read(null);
    let dataStream = Gio.DataInputStream.new(fileStream);
    let bytesReceive = 0;
    let bytesTransmit = 0;
    let bytes = 0;
    let line;
    while((line = dataStream.read_line(null)) != null) {
      line = String(line);
      line = line.trim();
      let column = line.split(/\W+/);
      if (column.length <= 2) break;
      if (column[0] != 'lo' &&
         !isNaN(parseInt(column[1])) &&
         !column[0].match(/^br[0-9]+/) &&
         !column[0].match(/^tun[0-9]+/) &&
         !column[0].match(/^tap[0-9]+/) &&
         !column[0].match(/^vnet[0-9]+/) &&
         !column[0].match(/^virbr[0-9]+/)) {
        bytesReceive = bytesReceive + parseInt(column[1]);
        bytesTransmit = bytesTransmit + parseInt(column[9]);
        bytes = bytesTransmit + bytesReceive;
      }
    }
    fileStream.close(null);
    if (prevBytes === 0.0) {
      prevBytes = bytes;
      prevBytesReceive = bytesReceive;
      prevBytesTransmit = bytesTransmit;
    }
    let speedReceive = (bytesReceive - prevBytesReceive) / (refreshTime * 1024.0); // Skip Bytes/Sec.
    let speedTransmit = (bytesTransmit - prevBytesTransmit) / (refreshTime * 1024.0); // Skip Bytes/Sec.
    let speed = (bytes - prevBytes) / (refreshTime * 1024.0); // Skip Bytes/Sec.
    // netSpeed.set_text(netSpeedFormat(speed, speedReceive, speedTransmit));
    prevBytesReceive = bytesReceive;
    prevBytesTransmit = bytesTransmit;
    prevBytes = bytes;

    file = Gio.file_new_for_path('/proc/meminfo');
    fileStream = file.read(null);
    dataStream = Gio.DataInputStream.new(fileStream);
    memTotal = 0;
    memAvailable = 0;
    while( ((line = dataStream.read_line(null)) != null) && ((memTotal == 0) || (memAvailable == 0)) ) {
      line = String(line);
      line = line.trim();
      let column = line.split(/\s+/);
      if (column.length <= 2) break;
      if (column[0] == 'MemTotal:' && !isNaN(parseInt(column[1]))) {
        memTotal = parseInt(column[1]);
      }
      if (column[0] == 'MemAvailable:' && !isNaN(parseInt(column[1]))) {
        memAvailable = parseInt(column[1]);
      }
    }
    fileStream.close(null);
    let memPercent=((memTotal-memAvailable)*100/memTotal).toFixed(0);
    let memArrow='';
    if (memPercent==prevMemPercent) {
      memArrow='≈';
    } else if (memPercent<prevMemPercent) {
      memArrow='↓';
    } else {
      memArrow='↑';
    }
    prevMemPercent=memPercent;
    
    file = Gio.file_new_for_path('/proc/stat');
    fileStream = file.read(null);
    dataStream = Gio.DataInputStream.new(fileStream);
    cpuTotal = 0;
    cpuIdle = 0;
    while((line = dataStream.read_line(null)) != null) {
      line = String(line);
      line = line.trim();
      let column = line.split(/\s+/);
      if (column[0]=='cpu') {
        column.shift();
        cpuIdle=column[3];
        for(let i=0; i<column.length; i++) {
          cpuTotal=cpuTotal+parseInt(column[i]);
        }
        break;
      }
    }
    fileStream.close(null);
    let DIFF_IDLE=cpuIdle-prevCpuIdle;
    let DIFF_TOTAL=cpuTotal-prevCpuTotal;
    let cpuPercent=((1000*(DIFF_TOTAL-DIFF_IDLE)/DIFF_TOTAL+5)/10).toFixed(0);
    let cpuArrow='';
    if (cpuPercent==prevCpuPercent) {
      cpuArrow='≈';
    } else if (cpuPercent<prevCpuPercent) {
      cpuArrow='↓';
    } else {
      cpuArrow='↑';
    }
    prevCpuPercent=cpuPercent;
    prevCpuTotal=cpuTotal;
    prevCpuIdle=cpuIdle;

    netSpeed.set_text("[ " + cpuPercent + "٪©" + cpuArrow + "  |  " + memPercent + "٪Ⓜ" + memArrow + "  |  " + netSpeedFormat(speed, speedReceive, speedTransmit) + " ]");
  } catch(e) {
    netSpeed.set_text(e.message);
  }
  return true;
}

function netSpeedFormat(speed, speedReceive, speedTransmit) {
  let units = ["KB", "MB", "GB", "TB"];
  if (speed === 0.0) {
    return "0.00" + units[0] + "⇅  0.00" + units[0] + "↓  0.00" + units[0] + "↑";
  }
  let iSpeed = 0;
  while(speed >= 1024.0) {  // Convert KB, MB, GB, TB
    speed /= 1024.0;        // 1 MegaBytes = 1024 KiloBytes
    iSpeed++;
  }
  let iSpeedReceive = 0;
  while(speedReceive >= 1024.0) {  // Convert KB, MB, GB, TB
    speedReceive /= 1024.0;        // 1 MegaBytes = 1024 KiloBytes
    iSpeedReceive++;
  }
  let iSpeedTransmit = 0;
  while(speedTransmit >= 1024.0) {  // Convert KB, MB, GB, TB
    speedTransmit /= 1024.0;        // 1 MegaBytes = 1024 KiloBytes
    iSpeedTransmit++;
  }
  return String(speed.toFixed(2) + units[iSpeed] + "⇅  " + speedReceive.toFixed(2) + units[iSpeedReceive] + "↓  " + speedTransmit.toFixed(2) + units[iSpeedTransmit] + "↑");
}

let button, timeout, netSpeed;
function init() {
  button = new St.Bin({
    style_class: 'panel-button',
    reactive: true,
    can_focus: false,
    x_fill: true,
    y_fill: false,
    track_hover: false
  });
  netSpeed = new St.Label({
    text: '⇅ -.-- --',
    style_class: 'netSpeedLabel'
  });
  button.set_child(netSpeed);
}

function enable() {
  Main.panel._rightBox.insert_child_at_index(button, 0);
  timeout = Mainloop.timeout_add_seconds(refreshTime, getNetSpeed);
}

function disable() {
  Mainloop.source_remove(timeout);
  Main.panel._rightBox.remove_child(button);
}
